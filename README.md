# 4002-intro-to-tech-stack

Well done! You're getting the hang of this.

So, you've probably figured by now that this repository contains the clue you're after...if only you could actually see it!

Fortunately, this git repo is hosted on GitLab, so you *could* browse its contents from the browser.

To find the URL of its remote origin, try:

	git remote get-url origin

And, should you find you need to commit anything to the remote, the passphrase for this client's SSH key is **goodfellow**. Just saying! ;-)

**NB:** Despite this repo having the same name as the other one you've encountered, they are **not** the same repo...they belong to different GitLab users. So don't get any ideas about totally screwing with the system!!

Oh, and one final little tip...pressing 'up' on the keyboard recalls the command history for this user on this system. Might be useful for you to know how this README file got to be on the remote, for example.
